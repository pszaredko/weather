import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CityCardComponent } from './components/city-card/city-card.component';
import { MatCardModule } from '@angular/material/card';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { OpenWeatherInterceptor } from './interceptors/open-weather.interceptor';
import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PreviewComponent } from './pages/preview/preview.component';
import { HourCardComponent } from './components/hour-card/hour-card.component';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { MatIconModule } from '@angular/material/icon';


registerLocaleData(localePl, 'pl');
@NgModule({
  declarations: [
    AppComponent,
    CityCardComponent,
    HomeComponent,
    NotFoundComponent,
    PreviewComponent,
    HourCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    HttpClientModule,
    MatIconModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: OpenWeatherInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
