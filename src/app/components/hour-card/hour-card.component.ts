import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hour-card',
  templateUrl: './hour-card.component.html',
  styleUrls: ['./hour-card.component.scss']
})
export class HourCardComponent implements OnInit {

  @Input() hour: any;

  constructor() { }

  ngOnInit(): void {
  }

}
