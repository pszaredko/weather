import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.scss']
})
export class CityCardComponent implements OnInit { 

  @Input() city: any;

  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
  }

  preview(): void {
    this._router.navigateByUrl(`preview/${this.city.name.toLowerCase()}/${this.city.coord.lat}/${this.city.coord.lon}`);
  }

}
