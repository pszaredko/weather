import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { WeatherResolverService } from './resolvers/weather-resolver.service';
import { PreviewComponent } from './pages/preview/preview.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {
      cities: WeatherResolverService
    }
  },
  {
    path: 'preview/:id/:lat/:lon',
    component: PreviewComponent,
    resolve: {
      onecall: WeatherResolverService,
    }
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
