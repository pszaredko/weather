import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { cities } from '../mocks/cities';
import { oWRC } from '../mocks/openWeatherReqConfig';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
   private _httpClient: HttpClient
  ) { }

  getWeatherData(): Observable<any> {
    const requests = [];
    cities.forEach(city => {
      requests.push(this._httpClient.get(`${environment.apiOpenWeather}/data/2.5/weather?q=${city.name}&units=${oWRC.units}&lang=${oWRC.lang}`))
    })
    return forkJoin(requests);
  }
  getWeatherHourlyData(params: any): Observable<any> {
    return this._httpClient.get(`${environment.apiOpenWeather}/data/2.5/onecall?lat=${params.lat}&lon=${params.lon}&exclude=${oWRC.onecallExclude.join(',')}&units=${oWRC.units}&lang=${oWRC.lang}`);
  }
}
