import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

const NUMBER_HOURS = 10;

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  hourly: any;
  city: string;
  NUMBER_HOURS = NUMBER_HOURS;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this._activatedRoute.data.subscribe(data => {
      this.hourly = data.onecall.hourly.slice(0, NUMBER_HOURS).map(hour => {return {...hour, dt: hour.dt * 1000}});
    });
    const city = this._location.path().split('/')[2];
    this.city = city.charAt(0).toUpperCase() + city.slice(1);
  }

  back(): void {
    this._location.back();
  }

}
