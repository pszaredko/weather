import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { WeatherService } from '../services/weather.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherResolverService implements Resolve<any>{

  constructor(
    private _weatherService: WeatherService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    if (route.url[0]?.path === 'preview') {
      return this._weatherService.getWeatherHourlyData(route.params);
    } else {
      return this._weatherService.getWeatherData();
    }
  }
}
